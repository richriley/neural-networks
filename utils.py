import numpy as np


def load_shape(shape_name):
    return np.load('resources/shapes/' + shape_name)